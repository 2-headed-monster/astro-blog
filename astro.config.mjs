import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";

import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  base: "/astro-blog",
  site: "https://2-headed-monster.gitlab.io",
  integrations: [mdx(), sitemap()],
  outDir: "public",
  publicDir: "static",
});
